<?xml version="1.0" encoding="utf-8"?>
<!-- Copyright 2019 George Florea Bănuș -->
<component type="desktop-application">
    <name>Manga Reader</name>
    <id>com.georgefb.mangareader</id>
    <launchable type="desktop-id">com.georgefb.mangareader.desktop</launchable>
    <metadata_license>CC-BY-4.0</metadata_license>
    <project_license>GPL-3.0-or-later</project_license>
    <url type="homepage">https://gitlab.com/g-fb/mangareader</url>
    <url type="bugtracker">https://gitlab.com/g-fb/mangareader/-/issues</url>
    <url type="donation">https://www.patreon.com/georgefb</url>
    <summary>Manga Reader for local files</summary>
    <description>
        <p>Manga reader for local files.</p>
        <p>Supports zip, rar, tar, 7z, cbz, cbr, cbt, cb7 files and also folders.</p>
        <p>Images are displayed in a vertical list and their size can be configured (fit width and/or height, user defined max width, original size).</p>
        <p>Can set up multiple manga folders whose contents will be displayed in a tree view, from where they can be opened.</p>
        <p>Can bookmark mangas.</p>
        <p>Configurable keyboard shortcuts.</p>
    </description>
    <releases>
        <release version="1.6.0" date="2021-12-14">
            <description>
                <p>Add option to change the color scheme. Can be accessed from the menubar, under Settings.</p>
            </description>
        </release>
        <release version="1.5.0" date="2021-03-10">
            <description>
                <p>Add option to resize image after a short delay, prevents unnecesary resizes when resizing the window or the sidebar.</p>
                <p>Changed text above the tree view to show the manga folder full path.</p>
                <p>Fixed an issues causing bad images quality.</p>
                <p>Fixed custom border color not being set.</p>
            </description>
        </release>
        <release version="1.4.1" date="2020-11-18">
            <description>
                <p>Fix custom shortcuts for the viewer (next/previous image, scroll up/down) not being saved.</p>
            </description>
        </release>
        <release version="1.4.0" date="2020-10-26">
            <description>
                <p>Use QImageReader to get the source sizes of images and use them to calculate the scaled sizes.</p>
                <p>Before, the sizes were estimated from the loaded images, causing some minor issues.</p>
                <p>Can now click and drag to scroll in the images viewer.</p>
                <p>Code and build improvements.</p>
            </description>
        </release>
        <release version="1.3.0" date="2020-05-11">
            <description>
                <p>Fixed a memory leak when loading a new manga (Mariusz Chryc)</p>
                <p>Add actions/keyboard shortcuts to navigate to the next (right key) and previous images (left key). (Mariusz Chryc)</p>
                <p>Add fit height and fit width options. (Mariusz Chryc)</p>
                <p>Changed single page zoom, now increases/decreases the current zoom level, rather than setting it to fixed values.</p>
            </description>
        </release>
        <release version="1.2.0" date="2020-03-25">
            <description>
                <p>Add zoom support.</p>
                <ul>
                    <li>middle click image to toggle zoom for that image</li>
                    <li>ctrl + scroll up/down (or ctrl + +/-) to zoom in/out all images</li>
                    <li>ctrl + 0 to reset zoom</li>
                    <li>actions (menu bar and tool bar) to zoom in/out</li>
                </ul>
                <p>Use unrar to extract rar files.</p>
                <p>Add actions to focus the manga tree, bookmaks table and manga view</p>
                <ul>
                    <li>manga tree 'ctrl+n' or 'n'</li>
                    <li>bookmarks table 'ctrl+b' or 'b'</li>
                    <li>manga view 'ctrl+v' or 'v'</li>
                </ul>
                <p>Open manga with enter from manga tree and bookmarks table.</p>
                <p>Set open file/folder as window title.</p>
                <p>Remember menu bar and tool bar visibility.</p>
                <p>Fix manga tree and bookmarks table resizing when toggling to fullscreen and back.</p>
            </description>
        </release>
        <release version="1.1.0" date="2020-03-20">
            <description>
                <p>Added rename option to the context menu of the tree view item.</p>
                <p>Added context menu to bookmarks.</p>
                <p>Added go to page widget in the toolbar.</p>
                <p>Fixed bugs related to saving and deleting bookmarks.</p>
                <p>Fixed bugs related to deleting manga folders.</p>
            </description>
        </release>
        <release version="1.0.6" date="2020-03-18">
            <description>
                <p>Open archives and folders directly and through command line.</p>
                <p>Scroll with up and down arrow keys (keys can be changed).</p>
            </description>
        </release>
        <release version="1.0.5" date="2020-03-17">
            <description>
                <p>Fix recursive bookmarks not being deleted.</p>
            </description>
        </release>
        <release version="1.0.4" date="2019-10-27">
            <description>
                <p>Fix tree not being setup when first adding a manga folder.</p>
                <p>Fix not saving active manga folder after adding it.</p>
                <p>Fix crash when switching active manga folder, caused by the manga tree not being setup.</p>
            </description>
        </release>
        <release version="1.0.3" date="2019-10-26">
            <description>
                <p>Fix index out of range error when checking if previous and next pages are in view.</p>
            </description>
        </release>
        <release version="1.0.2" date="2019-09-25">
            <description>
                <p>Improved the adding and removing of manga folders.</p>
            </description>
        </release>
        <release version="1.0.1" date="2019-08-03">
            <description>
                <p>Show more archive types in the folder tree.</p>
                <p>Show page numbers instead of the index in the bookmarks table.</p>
                <p>Fixes to folder tree.</p>
            </description>
        </release>
        <release version="1.0.0" date="2019-07-07">
            <description>
                <p>First release.</p>
            </description>
        </release>
    </releases>
    <screenshots>
        <screenshot type="default">
            <caption>Manga Reader</caption>
            <image>https://i.imgur.com/neoL6n1.png</image>
        </screenshot>
        <screenshot type="default">
            <caption>Manga Reader settings dialog</caption>
            <image>https://i.imgur.com/NKIIHpY.png</image>
        </screenshot>
        <screenshot type="default">
            <caption>Manga Reader light theme</caption>
            <image>https://i.imgur.com/YHfnzdx.png</image>
        </screenshot>
    </screenshots>
    <content_rating type="oars-1.0" />
</component>
